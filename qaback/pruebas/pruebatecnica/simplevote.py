import time
from selenium import webdriver


url = 'http://127.0.0.1:8000/polls/1/'

#selectores:

check_Notmuch = '#choice1'
check_Thesky = '#choice2'
check_Lalala = '#choice3'
botton_vote = 'body > form > input[type=submit]:nth-child(11)'

#Abrir navegador
driver = webdriver.Chrome(executable_path='./drivers/chromedriver')

#Maximizar pantalla
driver.maximize_window()
driver.get(url)

#Acción de espera para lectura del formulario
time.sleep(5)

#Seleccione una opción
driver.find_element_by_css_selector(check_Notmuch).click()

#Seleccione botón "Vote"
driver.find_element_by_css_selector(botton_vote).click()

#Acción de espera
time.sleep(3)

#Cerrar
driver.quit()
